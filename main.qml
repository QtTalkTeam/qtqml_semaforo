import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    id: root

    visible: true
    width: 640
    height: 480
    title: qsTr("Semaforo")

    Item {

        id: myItem

        Rectangle{
            id:light1
            x:25
            y:15
            width: 100
            height: width
            radius: width/2
            color: "black";
            border.color: Qt.lighter(color, 1.1);

        }

        Rectangle{
            id:light2
            x:25
            y: 15 + 10 + light1.height
            width: 100
            height: width
            radius: width/2
            color: "black";
            border.color: Qt.lighter(color, 1.1);


        }


        states: [

            State {

                name: "stop"

                PropertyChanges {
                    target: light1;
                    color:"red"
                }
                PropertyChanges {
                    target: light2;
                    color:"green"
                }

                PropertyChanges {
                    target: light2;
                    width: 200
                }
            },
            State {

                name: "go"

                PropertyChanges {
                    target: light1;
                    color:"green"
                }
                PropertyChanges {
                    target: light2;
                    color:"red"
                }
            }
        ]

        //Initial State
        state : "";

        //State Transacion
        transitions: [
                Transition {
        //            from: "stop"; to: "go"
                      from: "go"; to: "stop"
        //            from: "*"; to: "*"
                    ColorAnimation { target: light1; properties: "color"; duration: 1000 }
                    ColorAnimation { target: light2; properties: "color"; duration: 1000 }

                }
            ]
    }




    MouseArea{
        id: mousearea

        anchors.fill: parent

        onClicked: myItem.state = (myItem.state=="stop" ? "go" : "stop")
    }

    Timer{
        interval: 2000; running: true; repeat: true
        onTriggered: myItem.state = (myItem.state=="stop" ? "go" : "stop")
    }

}
